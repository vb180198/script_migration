package com.example.demo;

import lombok.Data;

@Data
public class Result {
    String customerAccount;
    String refnum;
    String email;
    String Status;

}
