package com.example.demo;

import com.opencsv.CSVReader;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class apicall {
    public static void main(String[] args) {

        ArrayList<Modal> modalList = new ArrayList<>();
        ArrayList<String> processedEmailIds = new ArrayList<>();
        List<String> tokens = getTokens();

        // Skipping 5 tokens for header.
        for (int i = 5; i < tokens.size(); i = i + 5) {

            String customerAccount = tokens.get(i);
            String refnum = tokens.get(i + 1);

            ArrayList<String> emailIds = new ArrayList();
            emailIds.addAll(readEmailAddress(tokens.get(i + 2)));
            emailIds.addAll(readEmailAddress(tokens.get(i + 3)));
            emailIds.addAll(readEmailAddress(tokens.get(i + 4)));

            modalList.addAll(emailIds.stream().map(email -> {
                Modal modal = new Modal(customerAccount, refnum, email, null);
                System.out.println("Record: " + modal.toString());
                return modal;
            }).collect(Collectors.toList()));


            processedEmailIds.addAll(emailIds);
        }

        System.out.println("Total processed usernames are :" + processedEmailIds.size());
        System.out.println("\n\n");

        for (Modal modal : modalList) {
            System.out.println("Processing modal - " + modal.toString());
            String ref_num = modal.getRefnum();
            String email = modal.getEmail();
                System.out.println("Processing email - " + email);
                if (email.contains("@")) {
                    //System.out.println(email);
                    try {
                        RestTemplate restTemplate = new RestTemplate();


                        SimpleClientHttpRequestFactory connFactory = new SimpleClientHttpRequestFactory();
                        connFactory.setConnectTimeout(60*1000);
                        connFactory.setReadTimeout(60*1000);
                        restTemplate.setRequestFactory(connFactory);

                        HttpHeaders headers = getHttpHeaders();

                        ResponseEntity<Response<UserDetailDTO>> resp = getUserDetails(ref_num, email, restTemplate, headers);
                        Response<UserDetailDTO> userDetailResponse = resp.getBody();

                        if (!userDetailResponse.getStatus() || userDetailResponse.getData() == null) {
                            System.out.println("1: User update failed for - " + email + ", Reason: " + userDetailResponse.getErrorDesc());
                            modal.setStatus("FAIL");
                            System.out.println("\n\n");
                            continue;
                        }


                        System.out.println("User Details for user = " + email);
                        System.out.println(userDetailResponse.getData().toString());
                        String id = userDetailResponse.getData().getId();

                        for (String tenant : userDetailResponse.getData().getAccessableTenants()) {
                            if (userDetailResponse.getData().getTenantsClientRoles().containsKey(tenant)) {
                                userDetailResponse.getData().getTenantsClientRoles().get(tenant).put(tenant.toLowerCase() + "-onephenom-api", Collections.singletonList("Client Admin"));
                            } else {
                                Map<String, List<String>> map = new LinkedHashMap<>();
                                map.put(tenant.toLowerCase() + "-onephenom-api", Collections.singletonList("Client Admin"));
                                userDetailResponse.getData().getTenantsClientRoles().put(tenant, map);
                            }
                        }


                        resp = updateUser(ref_num, restTemplate, headers, userDetailResponse, id);
                        Response<UserDetailDTO> updateResponse = resp.getBody();
                        if (!updateResponse.getStatus() || updateResponse.getData() == null) {
                            System.out.println("1: User update failed for - " + email + ", Reason: " + updateResponse.getErrorDesc());
                            modal.setStatus("FAIL");
                            System.out.println("\n\n");
                            continue;
                        }

                        modal.setStatus("SUCCESS");
                        System.out.println("User update completed for - " + email);


                    } catch (Exception e) {
                        modal.setStatus("FAIL");
                        System.out.println("2: User update failed for - " + email + ", Reason: " + e.getMessage());

                    }

                }
            System.out.println("************************\n\n");
        }
        List<Modal> successfulRecords = modalList.stream()
                .filter(modal -> "SUCCESS".equalsIgnoreCase(modal.getStatus())).collect(Collectors.toList());
        final long successCount = successfulRecords.size();
        List<Modal> failureRecords = modalList.stream()
                .filter(modal -> modal.getStatus() == null || modal.getStatus().equalsIgnoreCase("FAIL")).collect(Collectors.toList());
        final long failureCount = failureRecords.size();

        System.out.println("***************************************************************************");
        System.out.println("Successful:");
        System.out.println();
        for(Modal modal : successfulRecords) {
            System.out.println(modal.getCustomerAccount() + "-" + modal.getRefnum() + "-" + modal.getEmail());
        }
        System.out.println("***************************************************************************");
        System.out.println("Failed:");
        System.out.println();
        System.out.println();
        for(Modal modal : failureRecords) {
            System.out.println(modal.getCustomerAccount() + "-" + modal.getRefnum() + "-" + modal.getEmail());
        }

        System.out.println("***************************************************************************");

        System.out.println("***************************************");
        System.out.println("Total:" + processedEmailIds.size());
        System.out.println("Successful:" + successCount);
        System.out.println("Failed:" + failureCount);
        System.out.println("***************************************");

    }

    private static ResponseEntity<Response<UserDetailDTO>> updateUser(final String ref_num, final RestTemplate restTemplate, final HttpHeaders headers, final Response<UserDetailDTO> userDetailResponse, final String id) {
        HttpEntity<UserDetailDTO> entity1 = new HttpEntity<>(userDetailResponse.getData(), headers);
        ResponseEntity<Response<UserDetailDTO>> response = restTemplate.exchange("https://servicehub.phenom.com/api/tenants/" + ref_num + "/users/" + id + "/update",
                HttpMethod.PUT,
                entity1,
                new ParameterizedTypeReference<>() {
                });

        return response;
    }

    private static ResponseEntity<Response<UserDetailDTO>> getUserDetails(final String ref_num, final String email, final RestTemplate restTemplate, final HttpHeaders headers) {
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<Response<UserDetailDTO>> response = restTemplate.exchange("https://servicehub.phenom.com/api/tenants/" + ref_num + "/users/byusername/" + email,
                HttpMethod.GET, entity,
                new ParameterizedTypeReference<>() {
                });


        //System.out.println("Get Response Object "+getResponseObject.getData());
        return response;
    }

    private static List<String> getTokens() {
        CSVReader reader = null;
        ArrayList<String> tokens = new ArrayList<>();
        try {
            //parsing a CSV file into CSVReader class constructor
            reader = new CSVReader(new FileReader("src/main/java/com/example/demo/customers - Sheet1.csv"));
            String[] nextLine;

            //reads one line at a time
            while ((nextLine = reader.readNext()) != null) {
                for (String token : nextLine) {

                    tokens.add(token);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return tokens;
    }

    private static HttpHeaders getHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJDOElVV0tEV1o1bkVTYmEtd0ozMGNPZEtfVEZxMVhkRjljdXVyWFFwemJNIn0.eyJleHAiOjE2MzM5NTcyMDgsImlhdCI6MTYzMzk1MzYwOCwianRpIjoiNjZlMmQxYzUtYmI4Ny00YmQzLWJjNTItMDUxODEzNWE1MzFlIiwiaXNzIjoiaHR0cHM6Ly9hdXRoLnBoZW5vbS5jb20vYXV0aC9yZWFsbXMvUGhlbm9tIiwiYXVkIjpbIm9uZXBoZW5vbS1hcGkiLCJhY2NvdW50Il0sInN1YiI6ImIwM2JhNjdmLTEwNGMtNGRkOC05NmJkLWI3NWMyMzM4MGY4NCIsInR5cCI6IkJlYXJlciIsImF6cCI6Im9uZXBoZW5vbS1taWdyYXRpb24iLCJzZXNzaW9uX3N0YXRlIjoiY2U0NzdmNTYtYmJkNi00ZjA1LWJjOGMtY2U3MmMwZjZmODNhIiwiYWNyIjoiMSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsib25lcGhlbm9tLWFwaSI6eyJyb2xlcyI6WyJ1bWFfcHJvdGVjdGlvbiIsIlViZXIgQWRtaW4iLCJQaGVub20gQXBwIl19LCJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6InBoZW5vbV9lbWFpbCBwaGVub21fcm9sZXMgcGhlbm9tX3Byb2ZpbGUgcGhlbm9tX3RlbmFudHMgcGhlbm9tX3Bob25lIiwiY2xpZW50SWQiOiJvbmVwaGVub20tbWlncmF0aW9uIiwiY2xpZW50SG9zdCI6IjQ5LjIwNS4yNTEuMjU0IiwiY2xpZW50QWRkcmVzcyI6IjQ5LjIwNS4yNTEuMjU0IiwidXNlckRldGFpbHMiOnsiaWQiOiJiMDNiYTY3Zi0xMDRjLTRkZDgtOTZiZC1iNzVjMjMzODBmODQiLCJ1c2VyTmFtZSI6InNlcnZpY2UtYWNjb3VudC1vbmVwaGVub20tbWlncmF0aW9uIn19.XeNJQWYO_Q7vtWMSaan0BH0LCqBBUeHgrrxFAdt30hEQTkq-kn3MOSxNBCR1xaN6efe9XyEJz0h6mjZtAugExx6osOeAp_Hc5FUeClD7TH4Pu4q08D58npjJ8n6_WTxeEFSZPNwSFHBmNpVaSwwn6fK25a4eEZSg9Hw2jPj-3Hdba9HTI9WOvgOvAwUspCxHYAoSBZho74wyPoP_2V7ipqoIkb4G5VLw13fi36M_DUWGNR497K1Fr56TDTSOGI4vuOxAUz1mW22hQd3kfyM3yFnyJErWxiEd8ZCYR3Ve9o0QAIIgMtmx0UkWWafI3MfSzM4S1Ih2G-AWF16QimJCPA");
        headers.set("ph-org-code", "PHE");
        headers.set("ph-org-type", "PARTNER");
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        return headers;
    }

    public static List<String> readEmailAddress(String data) {
        String lines[] = data.split("\\r?\\n");
        final List<String> emails = new ArrayList<>();
        for (int i = 0; i < lines.length; i++) {
            String line = lines[i];
            String[] array = line.split(" ");
            for (int j = 0; j < array.length; j++) {

                if (array[j].contains("@")) {
                    String str = array[j].replace(",", "").replace("<", "").replace(">", "").replace("(", "").replace(")", "").trim();
                    emails.add(str.trim());
                }
            }
        }
        return emails;
    }

}


