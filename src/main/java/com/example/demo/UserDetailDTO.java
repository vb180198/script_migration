package com.example.demo;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
//import com.phenom.onephenomcore.domain.user.User;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class UserDetailDTO {

    private String id;
    private String userName;
    private String firstName;
    private String lastName;
    private String realm;
    private String email;
    private Map<String, Map<String, List<String>>> tenantsClientRoles; // cliendid map
    private List<String> accessableTenants;
    private List<String> realmRoles;
    private String contact;
    private String userTitle;
    private String userDesc;
    private String smsNo;
    private Boolean enabled;
    private String profileImage;
    private String city;
    private String state;
    private String location;
    private String federationLink;
    private UserType userType;
    private Long lastSession;
    private String legacyUserId;
    private String userSource;
    private String userOrg;
    private String invitedBy;
    private String nativeTenant;
    private String preferredIDP;

}
