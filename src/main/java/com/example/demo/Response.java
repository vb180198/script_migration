package com.example.demo;

import java.io.Serializable;

public class Response<T> implements Serializable {

    private static final long serialVersionUID = 1000000000000000000L;

    private T data;

    private Boolean status;
    private String errorCode;
    private String errorDesc;
    private String warningDesc;

    public Response() {}

    public Response(T data, Boolean status) {
        this.data = data;
        this.status = status;
    }

    public Response(Boolean status, String errorDesc) {
        this.status = status;
        this.errorDesc = errorDesc;
    }

    public Response(T data, Boolean status, String errorCode, String errorDesc) {
        this.data = data;
        this.status = status;
        this.errorCode = errorCode;
        this.errorDesc = errorDesc;
    }

    public Response(T data, String errorCode, String errorDesc) {
        this.data = data;
        this.errorDesc = errorDesc;
        this.errorCode = errorCode;
    }

    public Response(T data, boolean status, String warningDesc) {
        this.data = data;
        this.status = status;
        this.warningDesc = warningDesc;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDesc() {
        return errorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }


    public String getWarningDesc() {
        return warningDesc;
    }

    public void setWarningDesc(String warningDesc) {
        this.warningDesc = warningDesc;
    }

    @Override
    public String toString() {
        return "Response{"
                + "data="
                + data
                + ", status='"
                + status
                + '\''
                + ", errorCode='"
                + errorCode
                + '\''
                + ", errorDesc='"
                + errorDesc
                + '\''
                + '}';
    }
}
